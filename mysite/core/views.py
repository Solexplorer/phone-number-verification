from django.shortcuts import render
from django.views.generic import TemplateView
from django.core.files.storage import FileSystemStorage

from .helper import Validate


class Home(TemplateView):
    template_name = 'home.html'


def upload(request):
    context = {}
    if request.method == 'POST' and request.FILES['document']:
        uploaded_file = request.FILES['document']
        fs = FileSystemStorage()
        name = fs.save(uploaded_file.name, uploaded_file)
        uploaded_file.open()
        data_set = uploaded_file.read().decode('UTF-8').splitlines()
        data_set = [numbers.split(",")[1] for numbers in data_set]

        vd = Validate(is_csv=True)

        list_data = vd.elaborate_csv(data_set)
        uploaded_file.close()

        context['urls'] = fs.url(name)
        context['data'] = [[x['orig_number'], x['is_correct'], x['correct_number'] if 'correct_number' in x else ''] for
                           x in list_data]

    return render(request, 'upload.html', context)


def verify_number(request):
    context = {}
    if request.method == 'POST':
        input_number = request.POST.get('phone_number')
        vd = Validate(is_csv=False)
        result = vd.elaborate_number(input_number)
        context['data'] = result

    return render(request, 'verify_number.html', context)
