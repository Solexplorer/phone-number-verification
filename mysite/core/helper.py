import re
import pymongo
from django.conf import settings


class Validate():

    def __init__(self, is_csv):
        # We only need to connect to the DB if it's a CSV otherwise it's useless
        if is_csv:
            self.username = settings.USER_DB
            self.password = settings.PASS_DB
            self.host = settings.HOST_DB
            self.db_name = settings.NAME_DB
            self.db_instance = self.connect_to_mongo(self.username, self.password, self.host, self.db_name)

    def connect_to_mongo(self, username, password, host, db_name):
        client = pymongo.MongoClient(
            f'mongodb+srv://{username}:{password}@{host}/{db_name}?retryWrites=true&w=majority')
        return client["phoneCollection"]

    def insert_to_mongo(self, insert_list):
        collection = self.db_instance.numbers
        if type(insert_list) == list:
            collection.insert_many(insert_list)

    def query_to_mongo(self, select_range):
        collection = self.db_instance.numbers
        if select_range:
            return collection.find({"orig_number": {"$in": select_range}})

    def elaborate_csv(self, data):
        csv_list = []
        # If we have the data in the DB, no need to validate the data again
        numbers = list(self.query_to_mongo(select_range=data))
        for ph_numbers in data:
            # Check that we have at least 1 digit in the string
            if not re.match(r"\d", ph_numbers):
                continue
            if not any(n['orig_number'] == ph_numbers for n in numbers):
                csv_list.append(self.validate_numbers(ph_numbers))
        if csv_list:
            self.insert_to_mongo(csv_list)
        return self.query_to_mongo(select_range=data)

    def elaborate_number(self, phone_number):
        # It's faster to elaborate it here rather than querying the DB
        return self.validate_numbers(phone_number)

    def validate_numbers(self, phone_number):
        pattern_ok = r'^(27)[0-9]{9}$'

        # Step 1: Check if this is a correct number
        if re.match(pattern_ok, phone_number):
            return {'orig_number': phone_number, 'is_correct': True}

        # Step 2: Check numbers that start with 2 and have a length of 11
        elif re.match(r"^(2)[0-9]{10}$", phone_number):
            correct_number = '27' + phone_number[2:]
            return {'orig_number': phone_number, 'is_correct': False, 'correct_number': correct_number}

        # Step 3: Check number that have a length of 9 and add to those numbers the prefix
        elif re.match(r"^[0-9]{9}$", phone_number):
            return {'orig_number': phone_number, 'is_correct': False, 'correct_number': ("%s%s" % ('27', phone_number))}

        # Step 4: The number is definitely incorrect, nothing to do here ¯\_(ツ)_/¯
        else:
            return {'orig_number': phone_number, 'is_correct': False}
