# Verify South African Numbers

[![Python Version](https://img.shields.io/badge/python-3.7-brightgreen.svg)](https://python.org)
[![Django Version](https://img.shields.io/badge/django-2.1-brightgreen.svg)](https://djangoproject.com)

The website is hosted at: https://south-africa-number.herokuapp.com/

## Running the Project Locally

First, clone the repository to your local machine:

```bash
git clone https://gitlab.com/Solexplorer/phone-number-verification.git
```

Install the requirements:

```bash
pip install -r requirements.txt
```

Fill out the environment variables

```bash
mv mysite/.env.example .env
nano .env
```

### Warning

- Add the `SECRET_KEY`, if you don't know how to generate it, use [this online tool](https://miniwebtool.com/django-secret-key-generator/)
- `DEBUG` should be set as `False` when deploying to production for security reasons, it's fine to set it as `True` if you are in development

# Database

For this project, I've used MongoDB so you have 2 options:

## Use MongoDB Atlas as your database host(Recommended)

- Register an account [here](https://www.mongodb.com/cloud/atlas/register) and choose the free tier.
- Follow the wizard to create a Database and add the username, password, host and database name to the `.env` file

## Use your local MongoDB

- Modify the code in [here](mysite/core/helper.py:19) for logging into the DB and add the variables to the `.env` file

Apply the migrations:

```bash
python manage.py migrate
```

Finally, run the development server:

```bash
python manage.py runserver
```

The project will be available at **127.0.0.1:8000**.


## License

The source code is released under the [MIT License](LICENSE).